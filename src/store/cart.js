import { createSlice } from "@reduxjs/toolkit";
import { uiActions } from "./ui";

const cartSlice = createSlice({
  name: "cart",
  initialState: {
    id: "main", // for json-server (yarn serve-db)
    items: [],
    totalQuantity: 0,
    totalPrice: 0,
  },
  reducers: {
    addItemToCart(state, action) {
      const itemData = action.payload;

      state.totalPrice += itemData.price;
      state.totalQuantity++;

      const existsBefore = state.items.find((item) => item.id === itemData.id);

      if (existsBefore) {
        existsBefore.quantity++;
        existsBefore.totalPrice += itemData.price;
        return;
      }

      state.items.push({
        id: itemData.id,
        name: itemData.title,
        price: itemData.price,
        quantity: 1,
        totalPrice: itemData.price,
      });
    },
    removeItemFromCart(state, action) {
      const id = action.payload;

      const existsBefore = state.items.find((item) => id === item.id);

      state.totalQuantity--;
      state.totalPrice -= existsBefore.price;

      if (existsBefore.quantity === 1) {
        state.items = state.items.filter((item) => id !== item.id);
        return;
      }

      existsBefore.quantity--;
      existsBefore.totalPrice -= existsBefore.price;
    },
  },
});

export const sendCartData = (cart) => {
  return async (dispatch) => {
    dispatch(
      uiActions.showNotification({
        status: "Pending",
        title: "Sending...",
        message: "Sending state data",
      })
    );

    const sendRequest = async () => {
      const response = await fetch("http://localhost:5000/carts/main", {
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
        },
        method: "PUT",
        body: JSON.stringify(cart),
      });

      if (!response.ok) {
        throw new Error("Sending state data failed!");
      }
    };

    try {
      await sendRequest();

      dispatch(
        uiActions.showNotification({
          status: "success",
          title: "Success!",
          message: "Sent state data successfully!",
        })
      );
    } catch (error) {
      dispatch(
        uiActions.showNotification({
          status: "error",
          title: "Error!",
          message: error.message,
        })
      );
    }
  };
};

export const cartActions = cartSlice.actions;

export default cartSlice;
